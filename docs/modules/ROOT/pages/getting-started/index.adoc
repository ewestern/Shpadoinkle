= Getting Started


* xref:getting-started/extend-an-example.adoc[Extend an Example]
* xref:getting-started/adding-to-your-project.adoc[Adding to a Project]

Shpadoinkle is built with Nix. While it is possible to use Shpadoinkle without Nix, this project makes Nix the first class citizen. Nix allows us to make builds of Shpadoinkle as reproducible and deterministic as possible, by pinning every aspect of the system.

[TIP]
The fastest way to start a new Shpadoinkle project is to build a https://gitlab.com/fresheyeball/Shpadoinkle-snowman/-/tree/master#snowman[Snowman].

== Nix

Nix works on all Linux distributions, as well as Darwin-based operating systems such as MacOS. To install Nix simply run

[source,bash]
----
curl -L https://nixos.org/nix/install | sh
----

[WARNING]
Make sure to follow the instructions output by the script.

Success is indicated by the presence of the Nix toolchain present in your terminal...

[source,bash]
----
nix --version
----


== Cachix

Some of the work done with Nix includes deviations for the official Nix package set. As such some dependencies are not cached on https://cache.nixos.org. Building these dependencies from source can be very slow. If you would like to avoid this wait, you can pull from our https://cachix.org/[Cachix] cache of pre-built dependencies.

[source,bash]
----
nix-env -iA cachix -f https://cachix.org/api/v1/install
cachix use shpadoinkle
----

Now builds of Shpadoinkle will used cached dependencies from https://shpadoinkle.cachix.org/[shpadoinkle.cachix.org], which are kept up to date with `master` via Gitlab CI.

