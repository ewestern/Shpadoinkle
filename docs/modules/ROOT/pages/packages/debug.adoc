= Debug

This package contains tools for debugging Shpadoinkle applications. This package is a work in progress, but already contains some handy tools.

== Trapping

Tracking state over time, and logging it for inspection. This exposes two type classes:

[source,haskell]
----
class LogJS (c :: Type -> Constraint) where
  logJS :: c a => a -> JSM ()

class LogJS c => Trapper c where
  trapper :: c a => JSContextRef -> a -> a
----

These provide a `TypeApplications` interface for logging to the JavaScript console. For example you can log the state as it enters the `view` function thusly:


[source,haskell]
----
main :: IO ()
main = runJSorWarp 8080 $ do
  ctx <- askJSM
  simple runParDiff initial (view . trapper @ToJSON ctx) getBody
----

This will log all state by first encoding to JSON with Aeson, then then logging with `JSON.parse` so the browser console has the nice native display. If we change it to `trapper @Show ctx` it will use the `Show` instance instead.

== Chrome Dev Tools Extension

Planned features:

Exporting::
Export debugging session history to file.

Importing::
Step through playback of exported state.

Time Travel::
Rewind the application state, with support for branching timelines (DAG)

Outlines::
Event listener location visual callouts.


