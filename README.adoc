[.text-center]
image::docs/modules/ROOT/assets/images/logo.png[Eddie is loved forever,150,150]
[.text-center]
🤠 ⟶ 🥔 🤠

[.text-center]
==== I think I know precisely what I mean...

https://gitlab.com/fresheyeball/Shpadoinkle/commits/master[image:https://gitlab.com/fresheyeball/Shpadoinkle/badges/master/pipeline.svg[pipeline
status]]
https://opensource.org/licenses/BSD-3-Clause[image:https://img.shields.io/badge/License-BSD%203--Clause-blue.svg[BSD-3]]
https://builtwithnix.org[image:https://img.shields.io/badge/built%20with-nix-41439a[built
with nix]]
http://shpadoinkle.cachix.org[image:https://img.shields.io/badge/Cachix-up%20to%20date-green[cachix]]

= Shpadoinkle

https://www.youtube.com/watch?v=0CizU8aB3c8[Shpadoinkle] is a
programming model for user interface development. It supports flexible,
simple, declarative code by modeling user interface interactions as
Coalgebras. Currently builds and runs with *ghc 8.6.x* and *ghcjs 8.6.x*.

== Documentation

* http://fresheyeball.gitlab.io/Shpadoinkle/docs/index.html[Home Page]
* https://fresheyeball.gitlab.io/Shpadoinkle/docs/getting-started/index.html[Getting Started]
* https://fresheyeball.gitlab.io/Shpadoinkle/docs/tutorial/index.html[Tutorial]
* https://fresheyeball.gitlab.io/Shpadoinkle/docs/getting-started/extend-an-example.html[Extending an Example]

== Snowman

The easiest way to start a new Shpadoinkle project is to build a https://gitlab.com/fresheyeball/Shpadoinkle-snowman/-/tree/master#snowman[Snowman].

== TODOMVC

You can see an example of Shpadoinkle in the popular http://todomvc.com/[TODOMVC] running http://fresheyeball.gitlab.io/Shpadoinkle/examples/todomvc.jsexe[here], compiled from the code located https://gitlab.com/fresheyeball/Shpadoinkle/-/blob/master/examples/TODOMVC.hs[here].

== Hackage Matrix

[options="header"]
|===
|Package |Version |Dependencies |Availablity
|Core
|https://hackage.haskell.org/package/Shpadoinkle[image:https://img.shields.io/hackage/v/Shpadoinkle.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle[image:https://img.shields.io/hackage-deps/v/Shpadoinkle.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle/badge[Hackage
CI]]

|Html
|https://hackage.haskell.org/package/Shpadoinkle-html[image:https://img.shields.io/hackage/v/Shpadoinkle-html.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-html[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-html.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-html[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-html/badge[Hackage
CI]]

|Static
|https://hackage.haskell.org/package/Shpadoinkle-backend-static[image:https://img.shields.io/hackage/v/Shpadoinkle-backend-static.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-backend-static[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-backend-static.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-backend-static[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-backend-static/badge[Hackage
CI]]

|ParDiff
|https://hackage.haskell.org/package/Shpadoinkle-backend-pardiff[image:https://img.shields.io/hackage/v/Shpadoinkle-backend-pardiff.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-backend-pardiff[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-backend-pardiff.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-backend-pardiff[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-backend-pardiff/badge[Hackage
CI]]

|Snabbdom
|https://hackage.haskell.org/package/Shpadoinkle-backend-snabbdom[image:https://img.shields.io/hackage/v/Shpadoinkle-backend-snabbdom.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-backend-snabbdom[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-backend-snabbdom.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-backend-snabbdom[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-backend-snabbdom/badge[Hackage
CI]]

|Router
|https://hackage.haskell.org/package/Shpadoinkle-router[image:https://img.shields.io/hackage/v/Shpadoinkle-router.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-router[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-router.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-backend-snabbdom[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-backend-snabbdom/badge[Hackage
CI]]

|Widgets
|https://hackage.haskell.org/package/Shpadoinkle-widgets[image:https://img.shields.io/hackage/v/Shpadoinkle-widgets.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-widgets[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-widgets.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-widgets[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-widgets/badge[Hackage
CI]]

|Examples
|https://hackage.haskell.org/package/Shpadoinkle-examples[image:https://img.shields.io/hackage/v/Shpadoinkle-examples.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-examples[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-examples.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-widgets[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-widgets/badge[Hackage
CI]]

|Debug
|https://hackage.haskell.org/package/Shpadoinkle-debug[image:https://img.shields.io/hackage/v/Shpadoinkle-debug.svg[Hackage]]
|http://packdeps.haskellers.com/reverse/Shpadoinkle-debug[image:https://img.shields.io/hackage-deps/v/Shpadoinkle-debug.svg[Hackage
Deps]]
|https://matrix.hackage.haskell.org/#/package/Shpadoinkle-debug[image:https://matrix.hackage.haskell.org/api/v2/packages/Shpadoinkle-debug/badge[Hackage
CI]]
|===
